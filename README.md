### How to
- Clone the repository
- Run ```./build.sh && ./run.sh```
- URL where the application runs will be displayed
- For running *expressions.ypnb* copy *NKI_expression.tsv / PIPELINE_expression.tsv* to the container's working directory
