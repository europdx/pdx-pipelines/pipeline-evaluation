FROM ubuntu:18.04

USER root

ENV DEBIAN_FRONTEND=noninteractive 
ENV TZ=Europe/Prague

RUN apt update && apt install -y python3-pip

RUN pip3 install notebook matplotlib
RUN pip3 install pandas
RUN pip3 install plotly
RUN pip3 install cufflinks

RUN apt clean

WORKDIR /tmp
ENV HOME /tmp
