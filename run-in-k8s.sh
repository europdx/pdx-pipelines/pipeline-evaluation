#!/bin/bash

set -- $(getopt xn:l:t: "$@")

label=
name=edirex-pipeline-compare
image=ljocha/pipeline-eval:latest

token=$(LC_CTYPE=C tr -cd '[:alnum:]' </dev/urandom | head -c 30)

delete=0
delete_volume=0
while [ $1 != -- ]; do case $1 in
	-n) ns="-n $2"; nsname=$2; shift ;;
	-l) label=-$2; shift ;;
	-t) token=$2; shift ;;
	-x) delete=1 ;;
	esac
	shift
done

[ "$1" = -- ] && shift

if [ $delete = 1 ]; then
	kubectl delete deployment.apps/$name$label $ns
	kubectl delete service/$name-svc$label $ns
	kubectl delete ingress.networking.k8s.io/$name-ingress$label $ns
	exit 0
fi


#cat - <<EOF
kubectl apply $ns -f - <<EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: $name$label
spec:
  replicas: 1
  selector:
    matchLabels:
      app: $name$label
  template:
    metadata:
      labels:
        app: $name$label
    spec:
      securityContext:
        runAsUser: 1001
        runAsGroup: 1002
      containers:      
      - name: $name
        image: $image
        securityContext:
          allowPrivilegeEscalation: false
        ports:
          - containerPort: 9000
        workingDir: /tmp
        resources:
          requests:
            cpu: .2
          limits:
            cpu: 1
        command: ['jupyter', 'notebook', '--ip', '0.0.0.0', '--port', '9000', '--NotebookApp.token=$token' ]
EOF

kubectl apply $ns -f - <<EOF
apiVersion: networking.k8s.io/v1                                                
kind: Ingress                                                                   
metadata:                                                                       
  name: $name-ingress$label
  annotations:                                                                  
    kuberentes.io/ingress.class: "nginx"                                        
    kubernetes.io/tls-acme: "true"                                              
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
    external-dns.alpha.kubernetes.io/target: k8s-public-u.cerit-sc.cz
    nginx.ingress.kubernetes.io/proxy-body-size: 300m 
spec:                                                                           
  tls:                                                                          
    - hosts:                                                                    
        - "$name$label.dyn.cerit-sc.cz"                                              
      secretName: $name$label-dyn-cerit-sc-cz-tls                                    
  rules:                                                                        
  - host: "$name$label.dyn.cerit-sc.cz"                                          
    http:                                                                       
      paths:                                                                    
      - backend:                                                                
          service:                                                              
            name: $name-svc$label
            port:                                                               
              number: 80                                                      
        pathType: ImplementationSpecific
EOF

kubectl apply $ns -f - <<EOF
apiVersion: v1
kind: Service
metadata:
  name: $name-svc$label
spec:
  type: ClusterIP
  ports:
  - name: $name-port                                                 
    port: 80                                                                    
    targetPort: 9000    
  selector:
    app: $name$label
EOF

pod=($(kubectl get pods -n krenek-ns | grep $name))

[ -z "$pod" ] && { echo Something wrong >&2; exit 1; }

echo -n Waiting for pod/${pod[0]} to start

kubectl get pods $ns | grep $name | grep Running >/dev/null
running=$?

while [ $running -ne 0 ]; do
	echo -n .
	sleep 2
	kubectl get pods $ns | grep $name | grep Running >/dev/null
	running=$?
done
echo

for f in expressions.ipynb "$@"; do
	kubectl cp $f $nsname/${pod[0]}:/tmp/$f
done

echo 
echo https://$name$label.dyn.cerit-sc.cz/?token=$token
