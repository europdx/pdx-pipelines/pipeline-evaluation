#!/bin/sh

port=8888

docker run -p $port:$port -u $(id -u):$(id -g) -ti -v $PWD:/work edirex/pipeline-eval jupyter notebook --ip 0.0.0.0 --port $port
